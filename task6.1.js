const onMyBirthDay = (isKayoSick) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (!isKayoSick) {
                resolve(1);
            } else {
                reject(new Error("I am sad"));
            }
        }, 2000);
    });
};

console.time("Timer");


onMyBirthDay(false)
    .then((result) => {
        console.log("Timer");
        console.log(`I have ${result} cakes`);
    })
    .catch((error) => {
        console.timeLog("Timer");
        console.log(error);
    })
    .finally(() => {
        console.log("Party");
    })