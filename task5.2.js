const personList = [
	{ name: 'Greg', age: 50 },
	{ name: 'Sarah', age: 40 },
	{ name: 'Walden', age: 7 },
	{ name: 'Cosmos', age: 10 },
	{ name: 'Griffin', age: 5 },
	{ name: 'Corene', age: 35 },
]


//descending sort
function descSort(a, b) {
	if (a.age < b.age)
		return 1
	if (a.age > b.age)
		return -1
	return 0
}
//ascending sort
function ascSort(a, b) {
	if (a.age > b.age)
		return 1
	if (a.age < b.age)
		return -1
	return 0
}

personList.sort(descSort)
console.log(personList)

personList.sort(ascSort)
console.log(personList)