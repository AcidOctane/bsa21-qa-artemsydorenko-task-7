//Iterative Solution:
function doublefactorial(n) {
    let res = 1;
    for (let i = n; i >= 0; i = i - 2) {
        if (i == 0 || i == 1)
            return res;
        else
            res *= i;
    }

    return res;
}

//Recursive Solution: 
function recursiveDoublefactorial(n) {
    {
        if (n == 0 || n == 1)
            return 1;
        return n * doublefactorial(n - 2);
    }

}

let n = 9
let m = 0
let k = 1

console.log(doublefactorial(n))
console.log(doublefactorial(m))
console.log(doublefactorial(k))

console.log(recursiveDoublefactorial(n))
console.log(recursiveDoublefactorial(m))
console.log(recursiveDoublefactorial(k))