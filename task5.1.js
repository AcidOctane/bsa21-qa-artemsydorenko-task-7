const person1 = {
    name: "Peter",
    age: 48
}

const person2 = {
    name: "Lois",
    age: 43
}

const person3 = {
    name: "Stewie",
    age: 1
}

const person4 = {
    name: "Bryan",
    age: 1
}


   // < Person 1 name > is < older | younger | the same age as> <Person 2 name >

function compareAge(a, b) {
    let answer = ''
    if (a.age < 1 || b.age < 1) {
        answer = 'Incorrect age'
    }
    else if (a.age > b.age) {
        answer = `${a.name} is older than ${b.name}`
    }
    else if (a.age < b.age) {
        answer = `${a.name} is younger than ${b.name}`
    }
    else {
        answer = `${a.name} is the same age as ${b.name}`
    }

    console.log(answer)
}

compareAge(person1, person2)
compareAge(person2, person1)
compareAge(person3, person4)