let a = [true, false, true, true, false, true, true, true, false, false, true]
let b = [false, false]
let c = []

function countTrue(a) {
    let count = 0

    a.forEach(function (element) {
        if (element) {
            count++
        }
    })
    
    return count
}

console.log(countTrue(a))
console.log(countTrue(b))
console.log(countTrue(c))

